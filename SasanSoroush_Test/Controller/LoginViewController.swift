//
//  ViewController.swift
//  SasanSoroush_Test
//
//  Created by Sasan Soroush on 8/19/18.
//  Copyright © 2018 Sasan Soroush. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON

class LoginViewController: UIViewController {
    
    var fontSize : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()

    }
    
    //MARK:- UI components
    
    let logo : UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .clear
        view.image = #imageLiteral(resourceName: "w32")
        view.contentMode = UIViewContentMode.scaleAspectFit
        return view
    }()
    
    let customFBLoginButton : UIButton = {
        let button = UIButton(type: .system)
        button.clipsToBounds = true
        button.addTarget(self , action: #selector(customFBLoginButtonTapped), for: .touchUpInside)
        button.setBackgroundImage(#imageLiteral(resourceName: "SignInFB"), for: UIControlState.normal)
        return button
    }()
    
    let signInButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign into tipsy", for: UIControlState.normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitleColor(UIColor.init(rgb: 0xCAA8E5), for: UIControlState.normal)
        return button
    }()
    
    let signUpButton : UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Sign up", for: UIControlState.normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitleColor(UIColor.init(rgb: 0xCAA8E5), for: UIControlState.normal)
        return button
    }()
    
    let dividerLine : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(rgb: 0xCAA8E5)
        return view
    }()
    
    //MARK:- Methods
    
    @objc private func customFBLoginButtonTapped () {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    private func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    let json = JSON(result ?? {})
                    
                    do {
                        let response = try JSONDecoder().decode(Facebook_User.self, from: json.rawData())
                        response.save()
                        
                        self.navigateToUserPage()
                        
                    } catch let err {
                        print(err)
                    }

                }
            })
        }
    }
    
    private func navigateToUserPage() {
        let userPageViewController = UserViewController()
        self.present(userPageViewController, animated: true, completion: nil)
    }
    
    private func setupView() {
        
        view.backgroundColor = UIColor.init(rgb: 0x23262d)

        view.addSubview(logo)
        view.addSubview(customFBLoginButton)
        view.addSubview(signUpButton)
        view.addSubview(dividerLine)
        view.addSubview(signInButton)
        
        
        logo.frame = CGRect(x: 0, y: view.frame.height/6, width: view.frame.width, height: view.frame.height/3)
        
        customFBLoginButton.frame = CGRect(x: 30, y: view.frame.height/4*3, width: view.frame.width - 60, height: (view.frame.width - 60)/5)
        customFBLoginButton.layer.cornerRadius = customFBLoginButton.frame.height/4

        signInButton.frame = CGRect(x: 30, y: view.frame.height/4*3 + (view.frame.width - 60)/5 , width: customFBLoginButton.frame.width/5*3, height: view.frame.height/14)

        signUpButton.frame = CGRect(x: 30 + customFBLoginButton.frame.width/5*3 , y: view.frame.height/4*3 + (view.frame.width - 60)/5, width: customFBLoginButton.frame.width/5*2, height: view.frame.height/14)
        
        dividerLine.frame = CGRect(x: 30 + customFBLoginButton.frame.width/5*3 , y: signUpButton.frame.minY + 8,width : 0.5, height: view.frame.height/14 - 16)
        
    }
    
    
    
}
