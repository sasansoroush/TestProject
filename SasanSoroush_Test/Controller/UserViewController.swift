//
//  UserViewController.swift
//  SasanSoroush_Test
//
//  Created by Sasan Soroush on 8/19/18.
//  Copyright © 2018 Sasan Soroush. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage
import FBSDKLoginKit


class UserViewController: UIViewController {
    
    var users : [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        setupView()
        
    }
    
    //MARK:- UI Components
    
    let name : UserCustomLabel = {
        let label = UserCustomLabel(frame: CGRect())
        return label
    }()
    
    let first_name : UserCustomLabel = {
        let label = UserCustomLabel(frame: CGRect())

        return label
    }()
    
    let last_name : UserCustomLabel = {
        let label = UserCustomLabel(frame: CGRect())

        return label
    }()
    
    let email : UserCustomLabel = {
        let label = UserCustomLabel(frame: CGRect())

        return label
    }()
    
    let userImage : UIImageView = {
        let view = UIImageView()
        view.clipsToBounds = true
        return view
    }()
    
    let logoutBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Sign out", for: UIControlState.normal)
        btn.setTitleColor(.white, for: UIControlState.normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        btn.addTarget(self, action: #selector(logoutBtnTapped), for: UIControlEvents.touchUpInside)
        return btn
    }()
    
    //MARK:- Methods
    
    @objc private func logoutBtnTapped() {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut() // this is an instance function
        let vc = LoginViewController()
        self.present(vc, animated: true, completion: nil)
    }
        
    private func getData() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        do {
            users = try context.fetch(User.fetchRequest())
        } catch let err {
            print(err)
        }
    }
    
    private func setupView() {
        
        view.backgroundColor = UIColor.init(rgb: 0x23262d)

        view.addSubview(name)
        view.addSubview(first_name)
        view.addSubview(last_name)
        view.addSubview(email)
        view.addSubview(userImage)
        view.addSubview(logoutBtn)
        
        name.frame = CGRect(x: 0, y: view.frame.height/8 * 4, width: view.frame.width, height: view.frame.height/8)
        first_name.frame = CGRect(x: 0, y: view.frame.height/8 * 5, width: view.frame.width, height: view.frame.height/8)
        last_name.frame = CGRect(x: 0, y: view.frame.height/8 * 6, width: view.frame.width, height: view.frame.height/8)
        email.frame = CGRect(x: 0, y: view.frame.height/8 * 7, width: view.frame.width, height: view.frame.height/8)
        userImage.frame = CGRect(x: view.frame.width/4, y: view.frame.width/4  , width: view.frame.width/2, height: view.frame.width/2)
        userImage.layer.cornerRadius = view.frame.width/4
        logoutBtn.frame = CGRect(x: 20, y: 20, width: view.frame.width/4 , height: 40)
        
        guard let user = users.first else {name.text = "didn't found user" ; return}
        name.text = "Username : \(user.name!)"
        first_name.text = "First Name : \(user.first_name!)"
        last_name.text = "Last Name : \(user.last_name!)"
        email.text = "Email : \(user.email!)"
        userImage.sd_setImage(with: URL(string: user.imageURL!), completed: nil)
    }
}
