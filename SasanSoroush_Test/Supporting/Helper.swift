//
//  Helper.swift
//  SasanSoroush_Test
//
//  Created by Sasan Soroush on 8/19/18.
//  Copyright © 2018 Sasan Soroush. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Helper {
    static func deleteAllData(entity: String){
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = delegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do{
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
                delegate.saveContext()
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
}
