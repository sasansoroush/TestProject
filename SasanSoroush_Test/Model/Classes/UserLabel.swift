//
//  UserLabel.swift
//  SasanSoroush_Test
//
//  Created by Sasan Soroush on 8/19/18.
//  Copyright © 2018 Sasan Soroush. All rights reserved.
//

import UIKit

class UserCustomLabel : UILabel{
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textColor = .white
        font = UIFont.systemFont(ofSize: 20)
        textAlignment = .center
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
