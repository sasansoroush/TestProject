//
//  Facebook_User.swift
//  SasanSoroush_Test
//
//  Created by Sasan Soroush on 8/19/18.
//  Copyright © 2018 Sasan Soroush. All rights reserved.
//

import Foundation
import UIKit

struct Facebook_User : Decodable{
    let email : String
    let first_name : String
    let last_name : String
    let name : String
    let picture : pictureData
    
    func save () {

        Helper.deleteAllData(entity: "User")
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let context = delegate.persistentContainer.viewContext
        
        let user = User(context: context)
        user.name = self.name
        user.email = self.email
        user.first_name = self.first_name
        user.last_name = self.last_name
        user.imageURL = self.picture.data.url
        delegate.saveContext()
        
    }
    
}
