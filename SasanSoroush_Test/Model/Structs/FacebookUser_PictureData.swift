//
//  FacebookUser_PictureData.swift
//  SasanSoroush_Test
//
//  Created by Sasan Soroush on 8/19/18.
//  Copyright © 2018 Sasan Soroush. All rights reserved.
//

import Foundation

struct pictureData : Decodable {
    let data : FacebookUser_PictureData
}

struct FacebookUser_PictureData : Decodable{
    let height : Int?
    let width : Int?
    let is_silhouette : Bool
    let url : String
}
